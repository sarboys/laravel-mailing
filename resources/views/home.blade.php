@include('template.header')
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>schedule</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<div class="row mb-4">
		<div class="col-12">
			<h1 class="text-center">Расписание</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card p-4">
				<table class="table">
					<thead>
					<tr>
						<th scope="col">Название</th>
						<th scope="col">Статус</th>
						<th scope="col">Последняя отправка</th>
						<th scope="col">Следующая отправка</th>
						<th scope="col">Действия</th>
					</tr>
					</thead>
					<tbody>
					<tr id="firstSchedule">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
<script src="/js/index.js"></script>
</body>
</html>
@include('template.footer')