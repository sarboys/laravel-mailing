<?php

use App\Http\Controllers\MailingController;
use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('home');
});


Route::controller(\App\Http\Controllers\MailingController::class)->group(function () {
	Route::post('/mailing', 'index');
	Route::post('/mailing/edit/{mailing}', 'edit');
	Route::post('/mailing/delete/{mailing}', 'delete');
	Route::post('/mailing/start/{mailing}', 'start');
	Route::post('/mailing/stop/{mailing}', 'stop');

	Route::get('/mailing/all', 'all');
});

Route::controller(\App\Http\Controllers\BookController::class)->group(function () {
	Route::post('/book', 'index');
	Route::post('/book/edit/{book}', 'edit');
	Route::post('/book/delete/{book}', 'delete');
	Route::post('/book/stop/{book}', 'stop');

	Route::get('/book/all', 'all');
});


