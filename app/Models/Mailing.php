<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model
{
	protected $fillable= ['title','comment','start','status','template','ended','book_id'];

	public function books()
	{
		return $this->hasOne('App\Models\AddressBook','id');
	}
	static function checkStatus($id)
	{
		return self::find($id)->status;
	}
	static function setStatus($id,$status)
	{
		self::find($id)->update(
			['status' => $status]
		);
		return response()->json('success');
	}
}
