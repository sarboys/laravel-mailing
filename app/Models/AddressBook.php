<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    use HasFactory;
	protected $fillable= ['title','emails'];
	protected $casts = [
		'emails' => 'array'
	];
	public function mailings()
	{
		return $this->hasOne('App\Models\Mailing','book');
	}
}
