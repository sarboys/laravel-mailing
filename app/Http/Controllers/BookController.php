<?php

namespace App\Http\Controllers;

use App\Models\AddressBook;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class BookController extends Controller
{
	public function index (Request $request) {
		if ($request->file('file')->isValid()) {
			$file = Excel::toArray(['emails'], $request->file('file'));
		}
		AddressBook::create([
			'title' => $request->title,
			'emails' => json_encode(array_reduce($file[0], 'array_merge', array()))
		]);
		return response()->json(['Success'], 200);
	}
	public function edit(Request $request, AddressBook $book) {
		$book->update($request->all());
		return response()->json(['Success'], 200);
	}
	public function delete(AddressBook $book) {
		$book->delete();
		return response()->json(['Success'], 200);
	}
	public function all () {
		return AddressBook::all();
	}
}
