<?php

namespace App\Http\Controllers;

use App\Jobs\MailingQueueJob;
use App\Models\Mailing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MailingController extends Controller
{
	public function index (Request $request) {
		$mailing = Mailing::create([
			'title' => $request->title,
			'comment' =>  $request->comment,
			'status' => $request->status,
			'template' => $request->template,
			'ended' => $request->ended,
			'start' => Carbon::createFromFormat('Y-m-d H:m:s', $request->time)->format('Y-m-d H:m:s')
		]);
		return response()->json(array('success' => true, 'id' => $mailing->id), 200);
	}
	public function start(Mailing $mailing) {
		if($mailing->template != 0 && $mailing->book_id != 0 && $mailing->ended == 0)
			MailingQueueJob::dispatch($mailing);
		return response()->json(array('success' => true), 200);
	}
	public function edit(Request $request, Mailing $mailing) {
		$mailing->update($request->all());
		return [Mailing::setStatus($mailing->id,'test'),Mailing::checkStatus($mailing->id)];
		return response()->json(array('success' => true), 200);
	}
	public function delete(Mailing $mailing) {
		$mailing->delete();
		return response()->json(array('success' => true), 200);
	}
	public function all () {
		return Mailing::all();
	}
}
