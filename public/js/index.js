const $first = document.getElementById('firstSchedule')
moment.locale('ru')

const getRowElement = ( type, element ) => {
    if (!element || !type) return null
    const types = {
        "name": element.querySelector('td:nth-child(1)'),
        "status": element.querySelector('td:nth-child(2)'),
        "prevSending": element.querySelector('td:nth-child(3)'),
        "nextSending": element.querySelector('td:nth-child(4)'),
        "actions": element.querySelector('td:nth-child(5)')
    }
    return types[type] ? types[type] : null
}

const init = () => {
    getRowElement("name", $first).textContent = 'Тестовая рассылка'
    getRowElement("status", $first).innerHTML = '<span class="badge rounded-pill text-bg-success">Запущена</span>'
    getRowElement("prevSending", $first).textContent = moment('2022-07-20 08:45:32').fromNow()
    getRowElement("nextSending", $first).textContent = moment('2024-07-20 08:45:32').fromNow()
    getRowElement("actions", $first).innerHTML = '<button class="btn btn-primary">Редактировать</button><button class="btn btn-danger">Удалить</button>'
}

init()