# Laravel Mailing


## Что нужно для локального развертывания :
1. PHP >= 8.0.21 (Нужны модули включенные в php.ini : pdo_pgsql.dll)
2. Composer
3. Docker

## Установка :
1. git clone https://gitlab.com/sarboys/laravel_mailing.git
2. composer i
3. docker-compose up -d
